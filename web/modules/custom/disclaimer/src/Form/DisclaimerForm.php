<?php


namespace Drupal\disclaimer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Class DisclaimerForm.
 */
class DisclaimerForm extends FormBase {

  /**
   * Node object definition.
   *
   * @var \Drupal\node\Entity\Node
   */
  private $node;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disclaimer_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Node $node = NULL) {
    $text = $this->t('Disclaimer text.');

    $disclaimer_field_name = 'field_disclaimer';
    if ($node->hasField($disclaimer_field_name) && !$node->get($disclaimer_field_name)
        ->isEmpty()) {
      /** @var \Drupal\node\NodeInterface $disclaimer */
      $disclaimer = $node->get($disclaimer_field_name)->first()->entity;
      $text_field_name = 'body';
      if ($disclaimer->hasField($text_field_name) && !$disclaimer->get($text_field_name)
          ->isEmpty()) {
        $text = $disclaimer->get($text_field_name)
          ->first()
          ->getValue()['value'];
      }
    }

    $this->node = $node;
    $form['disclaimer'] = [
      '#markup' => $text,
    ];
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['decline'] = [
      '#type' => 'submit',
      '#value' => $this->t('Decline'),
      '#submit' => ['::disclaimerDecline'],
    ];
    $form['actions']['accept'] = [
      '#type' => 'submit',
      '#value' => $this->t('Accept'),
      '#submit' => ['::disclaimerAccepted'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function disclaimerAccepted(array &$form, FormStateInterface $form_state) {
    setrawcookie('SESSdisclosures' . $this->node->id(), rawurlencode(1), \Drupal::time()
        ->getRequestTime() + 31536000, '/');
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function disclaimerDecline(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('<front>');
  }
}
